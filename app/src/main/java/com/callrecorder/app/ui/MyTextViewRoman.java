package com.callrecorder.app.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Admin on 27.10.2015.
 */
public class MyTextViewRoman extends TextView {

    public MyTextViewRoman(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setType(context);
    }

    public MyTextViewRoman(Context context, AttributeSet attrs) {
        super(context, attrs);
        setType(context);
    }

    public MyTextViewRoman(Context context) {
        super(context);
        setType(context);
    }

    private void setType(Context context){
        this.setTypeface(Typeface.createFromAsset(context.getAssets(),
                "HelveticaNeueCyr-Roman.otf"));

        //this.setShadowLayer(1.5f, 5, 5, getContext().getResources().getColor(R.color.black_shadow));
    }
}
