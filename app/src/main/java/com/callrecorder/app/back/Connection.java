package com.callrecorder.app.back;

import android.content.Context;
import android.text.TextUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by dev on 29.07.2015.
 */
public class Connection {

    Context mContext;
    private RequestQueue mRequestQueue;
    private static Connection mInstance;
    public static final String TAG = Connection.class.getSimpleName();

    public static synchronized Connection getInstance(Context mContext) {
        if (mInstance == null)
            mInstance = new Connection(mContext);
        return mInstance;
    }

    public static void init (Context mContext){
        if (mInstance == null)
            mInstance = new Connection(mContext);
    }

    public Connection(Context mContext) {
        this.mContext = mContext;
    }

    public RequestQueue getRequestQueue() {

//        DefaultHttpClient mDefaultHttpClient = new DefaultHttpClient();

//        final ClientConnectionManager mClientConnectionManager = mDefaultHttpClient.getConnectionManager();
//        final HttpParams mHttpParams = mDefaultHttpClient.getParams();
//        final ThreadSafeClientConnManager mThreadSafeClientConnManager = new ThreadSafeClientConnManager( mHttpParams, mClientConnectionManager.getSchemeRegistry() );
//        mDefaultHttpClient = new DefaultHttpClient( mThreadSafeClientConnManager, mHttpParams );
//        final HttpStack httpStack = new HttpClientStack( mDefaultHttpClient );

//        DefaultHttpClient httpclient = new DefaultHttpClient();
//        CookieStore cookieStore = new BasicCookieStore();
//        httpclient.setCookieStore( cookieStore );
//        HttpStack httpStack = new HttpClientStack( httpclient );

//        CookieManager cookieManager = new CookieManager();
//        CookieHandler.setDefault(cookieManager);
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mContext); //, httpStack
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    /*public Response.ErrorListener getErrorListener(final Context context) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String gg = "d";
                if (error.toString().contains("TimeoutError") || // "com.android.volley.TimeoutError".equals(error.toString())
                        "com.android.volley.TimeoutError.NoConnectionError".equals(error.toString())) { //.contains("com.android.volley.TimeoutError.NoConnectionError")) {
                    String dd = "";
                    // Обработка ошибки
                } else if ("com.android.volley.AuthFailureError".equals(error.toString()) ||
                        error.toString().contains("No authentication") || error.toString().contains("Received authentication challenge is null")) {
                    // not autorithation 401
                    Preferences.get().eraseCookie();
                    Preferences.get().eraseTocken();
                    Preferences.get().eraseOther();
                    Preferences.get().eraseRegistration();
                    Intent intent = new Intent(context, Start.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    //((Activity)context).finish();
                } else {}
            }
        };
    }
    public Response.ErrorListener getErrorListener(final Context context, final ProgressDialog dialog) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String ddsd = "";
                if (error.toString().contains("TimeoutError") || // "com.android.volley.TimeoutError".equals(error.toString())
                        "com.android.volley.TimeoutError.NoConnectionError".equals(error.toString())) { //.contains("com.android.volley.TimeoutError.NoConnectionError")) {
                    String dd = "";
                    // Обработка ошибки
                } else if ("com.android.volley.AuthFailureError".equals(error.toString()) ||
                        error.toString().contains("No authentication")) {
                    // not autorithation 401
                    Preferences.get().eraseCookie();
                    Preferences.get().eraseTocken();
                    Preferences.get().eraseOther();
                    Intent intent = new Intent(context, Start.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
                String dfd = "";
                //dialog.dismiss();
            }
        };
    }*/
}