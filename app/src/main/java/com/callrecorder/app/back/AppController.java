package com.callrecorder.app.back;

import android.app.Application;
import java.util.HashMap;

/**
 * Created by Admin on 04.10.2015.
 */
public class AppController extends Application {

    public static int sort = 1;
    public static int minSum = 0;
    public static int currentRestaurantID = -1;

    public static boolean isSendingFile = false;

    @Override
    public void onCreate() {
        super.onCreate();
        Connection.init(getApplicationContext());
    }

    public static int currentRubric = 0;
    public static int currentCategory = 0;
    public static int rubricCount = 0;

    public static HashMap<Integer, Integer> basketMap = new HashMap<Integer, Integer>();
}
